﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

public class TableHeightControl : MonoBehaviour {

    [SerializeField] [Tooltip("The rigidbody of the table.")]
    private Rigidbody tableRigidbody;
    [SerializeField] [Tooltip("The speed of the control in m/s.")]
    private float speed = 0.02f;
    [SerializeField] [Tooltip("The maximum distance upwards the table can go.")]
    private float maxHeight = 0.3f;
    [SerializeField] [Tooltip("The maximum distance downwards the table can go.")]
    private float minHeight = -0.15f;
    [SerializeField] [Tooltip("The audio source of the table motor.")]
    private AudioSource tableMotorAudio;

    private Direction direction;
    public enum Direction
    {
        None,
        Up,
        Down
    }

    private float currentHeight = 0;
    private GameObject interactingController;

    public void SetDirection(Direction callingDirection, Direction direction, GameObject interactingController)
    {
        switch (direction)
        {
            case Direction.None:
                if (callingDirection == this.direction)
                    this.direction = direction;
                tableMotorAudio.Stop();
                break;

            case Direction.Up:
            case Direction.Down:
                this.direction = direction;
                tableMotorAudio.Play();
                break;
        }

        this.interactingController = interactingController;
    }

    private void FixedUpdate()
    {
        if (direction == Direction.None)
            return;

        float distance = speed * Time.fixedDeltaTime;

        if (direction == Direction.Up)
        {
            if (currentHeight <= maxHeight)
            {
                Vector3 pos = tableRigidbody.position;
                pos.y = pos.y + distance;

                tableRigidbody.MovePosition(pos);

                currentHeight += distance;

                if (VRTK_ControllerReference.GetControllerReference(interactingController) != null)
                    VRTK_ControllerHaptics.TriggerHapticPulse(VRTK_ControllerReference.GetControllerReference(interactingController), 0.3f);
            }
            else
            {
                tableMotorAudio.Stop();
            }
        }
        else if (direction == Direction.Down)
        {
            if (minHeight <= currentHeight)
            {
                Vector3 pos = tableRigidbody.position;
                pos.y = pos.y - distance;

                tableRigidbody.MovePosition(pos);

                currentHeight -= distance;

                if (VRTK_ControllerReference.GetControllerReference(interactingController) != null)
                    VRTK_ControllerHaptics.TriggerHapticPulse(VRTK_ControllerReference.GetControllerReference(interactingController), 0.3f);
            }
            else
            {
                tableMotorAudio.Stop();
            }
        }
    }
}
