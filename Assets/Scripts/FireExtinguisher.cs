﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

public class FireExtinguisher : MonoBehaviour {
    
    [SerializeField] [Tooltip("The particle system.")]
    private ParticleSystem particles;

    [SerializeField] [Tooltip("The audio.")]
    private AudioSource audio;


    private VRTK_InteractableObject interactableObject;

	// Use this for initialization
	void Start () {
        interactableObject = GetComponent<VRTK_InteractableObject>();

        interactableObject.InteractableObjectUsed += OnInteractableObjectUsed;
        interactableObject.InteractableObjectUnused += OnInteractableObjectUnused;
	}

    private void OnInteractableObjectUsed(object sender, InteractableObjectEventArgs e)
    {
        audio.Play();

        CancelInvoke();

        Invoke("StartParticles", 0.5f);        
    }

    private void OnInteractableObjectUnused(object sender, InteractableObjectEventArgs e)
    {
        particles.Stop();
        audio.Stop();
        CancelInvoke();
    }

    private void StartParticles()
    {
        particles.Play();
    }
}
