﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class AnalysisScreen : MonoBehaviour {
	public TextMeshProUGUI resultIndex;
    public TextMeshProUGUI durationTextMesh;
    public TextMeshProUGUI hesitationTextMesh;
    public TextMeshProUGUI transcriptTextMesh;
	public TableScript wordsPerMinuteTable;
	public TableScript slidesPerSeconds;

	public bool hideOnStart;
	public TextMeshProUGUI crowdTime;
	public TextMeshProUGUI crowdPercent;
	public TextMeshProUGUI notesTime;
	public TextMeshProUGUI notesPercent;
	public TextMeshProUGUI slidesTime;
	public TextMeshProUGUI slidesPercent;

	public TextMeshProUGUI headDistance;
	public TextMeshProUGUI rightDistance;
	public TextMeshProUGUI leftDistance;
	public TextMeshProUGUI advice;
	
	public TextMeshProUGUI[] mostCommonWords; 

	// Use this for initialization
	void Start() {
		if (hideOnStart)
		{
			EventSystem.endEvent.AddListener(ShowResults);
			EventSystem.startEvent.AddListener(HideResults);
			HideResults(0.0f);
		}
		
	}
	private void OnDestroy()
	{
		if(hideOnStart)
		{
			EventSystem.startEvent.RemoveListener(HideResults);
			EventSystem.endEvent.RemoveListener(ShowResults);
		}
		
	}

	// Update is called once per frame
	void Update() {

	}
	string GetTimeString(float time)
	{
		return $"{Mathf.Floor(time / 3600)}h {Mathf.Floor(time / 60)}m {Mathf.Floor(time % 60)}s";
	}
	public void ShowResults(Results result)
	{
		
		// Show all children
		for (int i = 0; i < transform.childCount; i++)
		{
			transform.GetChild(i).gameObject.SetActive(true);
		}
		// Set childrens stats to reflect results
		resultIndex.text = $"#{result.index}";
		durationTextMesh.text = GetTimeString(result.duration);
		hesitationTextMesh.text = result.audio.hesitationCount.ToString();
		int[] test = Enumerable.Range(1, result.audio.wordsPerMinute.Length).ToArray();
		int[] wordsPerMinute = result.audio.wordsPerMinute;
		wordsPerMinuteTable.SetData<int,int>(test,wordsPerMinute);
		 
		//Word repition

		for (int i = 0;
			i < result.audio.mostUsedWords.Length &&
			i < mostCommonWords.Length;
			i++)
		{
			mostCommonWords[i].text = $"{result.audio.mostUsedWords[i].Key} ({result.audio.mostUsedWords[i].Value})";
		}
		//Set focus times and percentage
		float total = result.movement.AudienceGazeDuration + result.movement.CueCardGazeDuration + result.movement.ScreenGazeDuration;
		crowdTime.text = GetTimeString(result.duration * (result.movement.AudienceGazeDuration / total));
		crowdPercent.text = $"{((result.movement.AudienceGazeDuration / total) * 100).ToString("00.00")}%";
		notesTime.text = GetTimeString(result.duration * (result.movement.CueCardGazeDuration / total));
		notesPercent.text = $"{((result.movement.CueCardGazeDuration / total) * 100).ToString("00.00")}%";
		slidesTime.text = GetTimeString(result.duration*(result.movement.ScreenGazeDuration / total));
		slidesPercent.text = $"{((result.movement.ScreenGazeDuration / total) * 100).ToString("00.00")}%";

		//Slides per second
		var data = result.slides.Times.Select((value, i) => new KeyValuePair<int, float>(i, value)).OrderByDescending(i => i.Value).Take(6);
		slidesPerSeconds.SetData(data.Select(kvp => kvp.Key).ToArray(), data.Select(kvp => kvp.Value.ToString("0#.##")).ToArray());


		//Set distance values
		headDistance.text = $"{result.movement.HeadDistanceTravelled.ToString("00.00")} M";
		leftDistance.text = $"{result.movement.LeftControllerDistanceTravelled.ToString("00.00")} M";
		rightDistance.text = $"{result.movement.RightControllerDistanceTravelled.ToString("00.00")} M";

		var totalMoved = result.movement.HeadDistanceTravelled + result.movement.LeftControllerDistanceTravelled + result.movement.RightControllerDistanceTravelled;
		var distancePerSecond = totalMoved / result.duration;
		// TotalMoved / duration is meters moved per second
		if (distancePerSecond < 0.1f)
			advice.text = "Move More";
		else if (distancePerSecond < 0.8f)
		{
			advice.text = "nothing";
		}
		else
		{
			advice.text = "Move Less";
		}

	}
	/// <summary>
	///  Hides the results page. 
	/// </summary>
	/// <param name="_">Required for the start event but is not used here</param>
	public void HideResults(float _)
	{
		// Show all children
		for (int i = 0; i < transform.childCount; i++)
		{
			transform.GetChild(i).gameObject.SetActive(false);
		}
	}
}
