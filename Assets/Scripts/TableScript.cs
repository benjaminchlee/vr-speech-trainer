﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using TMPro;

[ExecuteInEditMode]
public class TableScript : MonoBehaviour {
	[Header("Data")]
	[SerializeField]
	private string title = "";
	[SerializeField]
	private float height = 0;
	[SerializeField]
	private string xAxisName = "";
	[SerializeField]
	private string yAxisName = "";

	[Header("Children")]
	[SerializeField]
	private GameObject verticleAxis;
	[SerializeField]
	private TextMeshProUGUI TitleText;
	[SerializeField]
	private TextMeshProUGUI xAxisTitle;
	[SerializeField]
	private TextMeshProUGUI yAxisTitle;
	[SerializeField]
	private TextMeshProUGUI xData;
	[SerializeField]
	private TextMeshProUGUI yData;



	private void Awake()
	{
		TitleText.text = title;
		xAxisTitle.text = xAxisName;
		yAxisTitle.text = yAxisName;
		verticleAxis.GetComponent<RectTransform>().sizeDelta = new Vector2(6.0f, height-69.0f);
		GetComponent<RectTransform>().sizeDelta = new Vector2(271.5f, height);
	}

	public void SetData<XType, YType>(XType[] x, YType[] y)
	{
		xData.text = string.Join("\n", x);
		yData.text = string.Join("\n", y);
	}
}
