﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine;

[ExecuteInEditMode]
public class GraphScript : MonoBehaviour {
	
	private void Start()
	{
	}
	void Update()
	{
		ShowResult(new int[3] { 1, 2, 5 });
		print("Sad");
	}
	public void ShowResult(int[] values)
	{
		// Remove all current children
		for(int i = 0; i < transform.childCount; i++)
		{
			Destroy(transform.GetChild(i));
		}

		//Draw the axis

		// Build the bars
		int maxValue = Mathf.Max(values);

		for(int i = 0; i < values.Length; i++)
		{
			//Draw bar
			GameObject obj = new GameObject();
			var trans = obj.AddComponent<RectTransform>();
			Image image = obj.AddComponent<Image>();
			image.color = new Color(0, 0, 0);

			RectTransform rect = GetComponent<RectTransform>();
			trans.anchorMin = new Vector2(1, 0);
			trans.anchorMax = new Vector2(0, 1);
			trans.pivot = new Vector2(0.0f, 0.0f);
			trans.transform.parent = transform;
			trans.localScale = new Vector3(0.9f*(1.0f/values.Length), values[i]/maxValue*0.9f);
			trans.localPosition = new Vector3((trans.rect.width*trans.localScale.x) * i, 20.0f,0.0f);
			
			//Draw x axis value
			obj = new GameObject();
			trans = obj.AddComponent<RectTransform>();
			var text = obj.AddComponent<TextMeshProUGUI>();
			trans.parent = transform;
			trans.pivot = new Vector2(0, 0);
			trans.sizeDelta = new Vector2(100, 50);
			trans.localScale = new Vector3(0.9f * (1.0f / values.Length),0.5f);
			trans.localPosition = new Vector3((trans.rect.width * trans.localScale.x) * i, 0.0f);
			text.text = i.ToString();
			text.fontSize = 30;
			text.alignment = TextAlignmentOptions.Center;

			//Dray y value
			obj = new GameObject();
			trans = obj.AddComponent<RectTransform>();
			text = obj.AddComponent<TextMeshProUGUI>();
			trans.parent = transform;
			trans.pivot = new Vector2(0, 0);
			trans.sizeDelta = new Vector2(100, 100);
			trans.localScale = new Vector3(0.9f * (1.0f / values.Length), 0.5f);
			trans.localPosition = new Vector3((trans.rect.width * trans.localScale.x) * i, ((values[i] / maxValue)*rect.rect.height)+4.0f);
			text.text = values[i].ToString();
			text.fontSize = 30;
			text.alignment = TextAlignmentOptions.Center;


		}
	}
}
