﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OverheadLightManager : MonoBehaviour {

    [SerializeField] [Tooltip("The overhead lights.")]
    private Light[] lights;
    [SerializeField] [Tooltip("The overhead light bases.")]
    private Renderer[] bases;

    [SerializeField] [Tooltip("The time in seconds for the lights to turn off.")]
    private float time;

    private float originalIntensity;
    private float originalRange;
    private Color originalHDR;
    private Coroutine lightsCoroutine;

    private void Start()
    {
        originalIntensity = lights[0].intensity;
        originalRange = lights[0].range;
        originalHDR = bases[0].material.GetColor("_EmissionColor");

        EventSystem.startEvent.AddListener(SpeechStarted);
        EventSystem.endEvent.AddListener(SpeechEnded);
    }

    private void SpeechStarted(float _)
    {
        if (lightsCoroutine != null)
            StopCoroutine(lightsCoroutine);

        StartCoroutine(TransitionLights(0.2f, 8, new Color(0, 0, 0, 0.4f), time));
    }

    private void SpeechEnded(Results _)
    {
        if (lightsCoroutine != null)
            StopCoroutine(lightsCoroutine);

        StartCoroutine(TransitionLights(originalIntensity, originalRange, originalHDR, time));
    }

    private IEnumerator TransitionLights(float targetIntensity, float targetRange, Color targetHDR, float time)
    {
        float elapsed = 0;
        float startIntensity = lights[0].intensity;
        float startRange = lights[0].range;
        Color startHDR = bases[0].material.GetColor("_EmissionColor");

        while (elapsed < time)
        {
            float intensity = Mathf.Lerp(startIntensity, targetIntensity, elapsed / time);
            float range = Mathf.Lerp(startRange, targetRange, elapsed / time);
            Color HDRIntensity = Color.Lerp(startHDR, targetHDR, elapsed / time);

            foreach (Light light in lights)
            {
                light.intensity = intensity;
                light.range = range;
            }

            foreach (Renderer r in bases)
            {
                r.material.SetColor("_EmissionColor", HDRIntensity);
            }

            yield return null;
            elapsed += Time.deltaTime;
        }

        foreach (Light light in lights)
        {
            light.intensity = targetIntensity;
            light.range = targetRange;
        }

        foreach (Renderer r in bases)
        {
            r.material.SetColor("_EmissionColor", targetHDR);
        }
    }
}
