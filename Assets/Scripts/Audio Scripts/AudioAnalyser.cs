﻿using IBM.Watson.DeveloperCloud.Services.SpeechToText.v1;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public struct AudioResults
{
	
	public string transcript;
	public int hesitationCount;
	public int[] wordsPerMinute;
	public KeyValuePair<string,int>[] mostUsedWords;

	public AudioResults(string transcriptInput, int hesitations,int[] wpm, KeyValuePair<string, int>[] mostUsed)
	{
		transcript = transcriptInput;
		hesitationCount = hesitations;
		wordsPerMinute = wpm;
		mostUsedWords = mostUsed;
	}
}
public class AudioAnalyser : MonoBehaviour {
	float m_startTime;
	string m_transcript = "";
	int m_hesitationCount = 0;

	List<string> m_commonWords;
	Dictionary<string, int> m_wordRepition;

	//Word count for each 10 second interval
	List<int> m_frequencyValue;

	//Last line is last incomplete line
	private SpeechRecognitionAlternative currentLine;

	// Use this for initialization
	void Start () {
		//Set up data structures
		m_wordRepition = new Dictionary<string, int>();
		m_frequencyValue = new List<int>();
		m_commonWords = new List<string>();
		//Populate common words with dictionary entries
		string path = "Assets/Resources/dictionary.txt";
		StreamReader reader = new StreamReader(path);
		while (!reader.EndOfStream)
		{
			m_commonWords.Add(reader.ReadLine().Trim());
		}
		reader.Close();
		//Start listening for startEvent
		EventSystem.startEvent.AddListener(ResetTranscript);


	}
	public void SetLastCurrent(SpeechRecognitionAlternative last)
	{
		currentLine = last;
	}
	/// <summary>
	/// On destruction we need to remove the event listener on start event
	/// </summary>
	private void OnDestroy()
	{
		EventSystem.startEvent.RemoveListener(ResetTranscript);
	}

	// Update is called once per frame
	void Update () {
		
	}
	/// <summary>
	/// Will run on startEvent, resets the values
	/// </summary>
	/// <param name="startTime"></param>
	public void ResetTranscript(float startTime)
	{
		m_transcript = "";
		m_hesitationCount = 0;
		m_startTime = startTime;
		m_frequencyValue.Clear();
		for (int i = 0; i < 100; i++)
		{
			m_frequencyValue.Add(0);
		}
		m_wordRepition.Clear();
	}
	public AudioResults GetResults()
	{
		if(currentLine != null)
		{
			AddLine(currentLine);
		}
		int lastNonzeroIndex = 0;
		for(int i = m_frequencyValue.Count-1; i >= 0 ; i--)
		{
			if(m_frequencyValue[i] != 0)
			{
				lastNonzeroIndex = i;
				break;
			}
		}
		int[] wordsForEachMinute =
			(from value in m_frequencyValue.Where((_,i)=>i <= lastNonzeroIndex).Select((value, index) => new { Index = index, Value = value })
			group value by value.Index / 6 into minuteGroup
			select minuteGroup.Sum(x => x.Value)).ToArray();

		var mostUsedWords = m_wordRepition.ToList().OrderByDescending(i => i.Value).Take(6);
		
		return new AudioResults(
			m_transcript, 
			m_hesitationCount,
			wordsForEachMinute.ToArray(),
			mostUsedWords.ToArray()
		);
	}
	/// <summary>
	/// This function adds a SpeechRecognitionAlternative to the transcript
	/// </summary>
	/// <param name="result">The latest speech recognition alternative</param>
	public void AddLine(SpeechRecognitionAlternative line)
	{
		//Get final timestamp
		double finalTime = line.Timestamps[line.Timestamps.Length - 1].End;
		//Subtract from time of line added to get aproximate time the user started talking
		float startOfLineTime = (Time.time-(float)finalTime)-m_startTime;
		
		foreach( var timeStamp in line.Timestamps)
		{
			if (timeStamp.Word == "%HESITATION")
			{
				m_hesitationCount += 1;
			}
			else //If not a hesitation we want to add a word to our frequency and word repition tables
			{
				//Check if word is in the list of common words
				if (m_commonWords.BinarySearch(timeStamp.Word) < 0)
				{
					//if not in common Words check if is in word repitions
					if (m_wordRepition.ContainsKey(timeStamp.Word))
					{
						m_wordRepition[timeStamp.Word] += 1;
					}
					else
					{
						m_wordRepition.Add(timeStamp.Word , 1);
					}
				}


				//Start by getting the time of the phrase relative to the speech
				float time = startOfLineTime + (float)timeStamp.End;
				//find the 10 second interval for 
				int timeIndex = (int)(time / 10);
				
				m_frequencyValue[timeIndex] += 1;
				

			}
		}
		m_transcript += line.transcript+"\n";
	}
}
