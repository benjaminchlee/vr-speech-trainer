﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;
using System.Linq;

[System.Serializable]
public struct MovementResults
{
	public float HeadDistanceTravelled;
	public float LeftControllerDistanceTravelled;
	public float RightControllerDistanceTravelled;
	public float AudienceGazeDuration;
	public float CueCardGazeDuration;
	public float ScreenGazeDuration;
}

public class MovementManager : MonoBehaviour {

    // Singleton pattern
    public static MovementManager Instance { get; private set; }
    
    [SerializeField] [Tooltip("The frequency in which the user's movements are logged in seconds. The lower this value, the more accurate the results will be at the cost of performance.")]
    private float loggingInterval = 0.05f;
    [SerializeField] [Tooltip("The triggers which approximate that the user is looking at the audience. These triggers should be on layer 8.")]
    private Collider[] audienceTriggers;
    [SerializeField] [Tooltip("The triggers which approximate that the user is looking at the cue card. These triggers should be on layer 8.")]
    private Collider[] cueCardTriggers;
    [SerializeField] [Tooltip("The triggers which approximate that the user is looking at the screen. These triggers should be on layer 8.")]
    private Collider[] screenTriggers;

    private Transform head;
    private Transform leftController;
    private Transform rightController;

    private Vector3 previousHeadPosition;
    private Vector3 previousLeftControllerPosition;
    private Vector3 previousRightControllerPosition;


    [SerializeField]  // Show in inspector for debugging
    private MovementResults results;
    public MovementResults Results
    {
        get { return results; }
    }

    private void Awake()
    {
        // Singleton pattern
        if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
            return;
        }
        Instance = this;
        DontDestroyOnLoad(gameObject);
    }

    private void Start()
    {
		//Set up event listeners
		EventSystem.startEvent.AddListener(StartLogging);

        // Get references to transforms
        head = Camera.main.transform;
        leftController = VRTK_DeviceFinder.GetControllerLeftHand().transform;
        rightController = VRTK_DeviceFinder.GetControllerRightHand().transform;
    }

	private void OnDestroy()
	{
		EventSystem.startEvent.RemoveListener(StartLogging);
	}

	/// <summary>
	/// Begins logging the user's movements.
	/// </summary>
	public void StartLogging(float startTime)
    {
        if (!IsInvoking("LogMovement"))
        {
            results = new MovementResults();

            previousHeadPosition = head.position;
            previousLeftControllerPosition = leftController.position;
            previousRightControllerPosition = rightController.position;

            InvokeRepeating("LogMovement", loggingInterval, loggingInterval);
        }
    }

    /// <summary>
    /// Stops logging the user's movements
    /// </summary>
    public MovementResults StopLogging()
    {
        CancelInvoke("LogMovement");

        return results;
    }
    
    /// <summary>
    /// Increments values stored in the results based upon the user's positions when this function is called
    /// </summary>
    private void LogMovement()
    {
        // Increment distances
        results.HeadDistanceTravelled += Vector3.Distance(previousHeadPosition, head.position);
        results.LeftControllerDistanceTravelled += Vector3.Distance(previousLeftControllerPosition, leftController.position);
        results.RightControllerDistanceTravelled += Vector3.Distance(previousRightControllerPosition, rightController.position);

        // Increment user's gaze target
        RaycastHit hit;
        if (Physics.Raycast(head.position, head.forward, out hit, 10f, 1 << 8, QueryTriggerInteraction.Collide))
        {
            if (audienceTriggers.Contains(hit.collider))
            {
                results.AudienceGazeDuration += loggingInterval;
            }
            else if (cueCardTriggers.Contains(hit.collider))
            {
                results.CueCardGazeDuration += loggingInterval;
            }
            else if (screenTriggers.Contains(hit.collider))
            {
                results.ScreenGazeDuration += loggingInterval;
            }
        }

        // Save references to current values to use in the next log
        previousHeadPosition = head.position;
        previousLeftControllerPosition = leftController.position;
        previousRightControllerPosition = rightController.position;
    }
}
