﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

/// <summary>
/// In order for the hinge to not exhibit "floaty" behaviour when it is released, this script will automatically disable the isKinematic boolean on all
/// rigibodies when grabbed so that they can move correctly, and re-enable it when ungrabbed so that it freezes in place.
/// </summary>
public class MonitorHinge : MonoBehaviour {

    private VRTK_InteractableObject interactableObject;
    private Rigidbody[] rigidbodies;
    private bool isJointBroken = false;

    private void Start()
    {
        interactableObject = GetComponent<VRTK_InteractableObject>();
        rigidbodies = transform.parent.GetComponentsInChildren<Rigidbody>();

        interactableObject.InteractableObjectGrabbed += InteractableObjectGrabbed;
        interactableObject.InteractableObjectUngrabbed += InteractableObjectUngrabbed;
    }
    
    private void InteractableObjectGrabbed(object sender, InteractableObjectEventArgs e)
    {
        ToggleKinematics(false);
    }

    private void InteractableObjectUngrabbed(object sender, InteractableObjectEventArgs e)
    {
        if (!isJointBroken)
            ToggleKinematics(true);
        else
            ToggleKinematics(false);
    }

    private void ToggleKinematics(bool state)
    {
        foreach (Rigidbody rb in rigidbodies)
        {
            rb.isKinematic = state;
        }
    }
    
    private void OnJointBreak(float breakForce)
    {
        foreach (Rigidbody rb in rigidbodies)
        {
            rb.isKinematic = false;
            rb.gameObject.GetComponent<MonitorHinge>().JointBroken();
        }
    }

    public void JointBroken()
    {
        isJointBroken = true;
    }
}
