﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using VRTK;

[System.Serializable]
public struct SlidesResults
{
    public float[] Times;
}

[RequireComponent(typeof(LineRenderer))]
public class Clicker : MonoBehaviour {

    [SerializeField] [Tooltip("The folder within Resources that contains the user's presentation slides. The slides must be in an image format.")]
    private string pathToPresentation = "Presentation";
    [SerializeField] [Tooltip("The renderers of the screens that show the slides.")]
    private Renderer[] presentationScreens;
    [SerializeField] [Tooltip("The previous (left) button on the clicker.")]
    private GameObject previousButton;
    [SerializeField] [Tooltip("The next (right) button on the clicker.")]
    private GameObject nextButton;
    [SerializeField] [Tooltip("The colour of an un-touched button.")]
    private Color untouchedColor = Color.grey;
    [SerializeField] [Tooltip("The colour of a touched button.")]
    private Color touchedColor = Color.red;
    [SerializeField] [Tooltip("The strength of the vibration when the user changes the slide. This value must be between 0 and 1.")]
    private float vibrationStrength = 0.4f;

    private VRTK_InteractableObject interactableObject;
    private VRTK_ControllerEvents leftControllerEvents;
    private VRTK_ControllerEvents rightControllerEvents;
    private LineRenderer lineRenderer;

    private float touchedAngle;

    private Texture2D[] slides;
    private int NbSlides
    {
        get { return slides.Length; }
    }
    private int currentIndex = 0;

    [SerializeField]  // Show in inspector for debugging
    private SlidesResults results;
    public SlidesResults Results
    {
        get { return results; }
    }
    private float startTime;
    private bool isStarted = false;

    private bool hasSlideChanged = false;

    private void Start()
    {
        // Set up VRTK references
        interactableObject = GetComponent<VRTK_InteractableObject>();
        leftControllerEvents = VRTK_DeviceFinder.GetControllerLeftHand().GetComponent<VRTK_ControllerEvents>();
        rightControllerEvents = VRTK_DeviceFinder.GetControllerRightHand().GetComponent<VRTK_ControllerEvents>();

        // Set up line renderer
        lineRenderer = GetComponent<LineRenderer>();
        lineRenderer.enabled = false;
        lineRenderer.useWorldSpace = false;
        lineRenderer.positionCount = 2;
        lineRenderer.SetPosition(0, Vector3.zero);

        // Register events
        leftControllerEvents.TouchpadTouchStart += OnTouchpadAxisChanged;
        rightControllerEvents.TouchpadTouchStart += OnTouchpadAxisChanged;
        leftControllerEvents.TouchpadAxisChanged += OnTouchpadAxisChanged;
        rightControllerEvents.TouchpadAxisChanged += OnTouchpadAxisChanged;
        leftControllerEvents.TouchpadTouchEnd += OnTouchpadTouchEnd;
        rightControllerEvents.TouchpadTouchEnd += OnTouchpadTouchEnd;
        leftControllerEvents.GripClicked += OnGripClicked;
        rightControllerEvents.GripClicked += OnGripClicked;
        leftControllerEvents.GripUnclicked += OnGripUnclicked;
        rightControllerEvents.GripUnclicked += OnGripUnclicked;
        interactableObject.InteractableObjectUngrabbed += OnObjectUngrabbed;

        EventSystem.startEvent.AddListener(StartLogging);

        // Set colours of buttons
        ResetButtons();

        LoadSlides(pathToPresentation);
    }
	private void OnDestroy()
	{
		EventSystem.startEvent.RemoveListener(StartLogging);
	}
	private void Update()
    {
        if (lineRenderer != null && lineRenderer.enabled)
        {
            RaycastHit hit;
            if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, 100f, ~0, QueryTriggerInteraction.Ignore)) 
            {
                lineRenderer.SetPosition(1, transform.InverseTransformPoint(hit.point));
            }
        }
    }

    public void StartLogging(float time)
    {
        if (!isStarted)
        {
            isStarted = true;

            results = new SlidesResults();
            results.Times = new float[NbSlides];

            startTime = time;
        }
    }

    public SlidesResults StopLogging()
    {
        if (isStarted)
        {
            isStarted = false;

            results.Times[currentIndex] += (Time.time - startTime);
        }

        return results;
    }

    private void OnTouchpadAxisChanged(object sender, ControllerInteractionEventArgs e)
    {
        if (interactableObject.IsGrabbed() && interactableObject.GetGrabbingObject() == e.controllerReference.scriptAlias)
        {
            touchedAngle = e.touchpadAngle;
            UpdateTouchedButton();

            if (0.8 <= e.touchpadAxis.x)
            {
                if (!hasSlideChanged)
                {
                    NextSlide();
                    hasSlideChanged = true;
                    VRTK_ControllerHaptics.TriggerHapticPulse(e.controllerReference, vibrationStrength);
                }
            }
            else if (e.touchpadAxis.x <= -0.8)
            {
                if (!hasSlideChanged)
                {
                    PreviousSlide();
                    hasSlideChanged = true;
                    VRTK_ControllerHaptics.TriggerHapticPulse(e.controllerReference, vibrationStrength);
                }
            }
            else
            {
                hasSlideChanged = false;
            }
        }
    }

    private void OnTouchpadTouchEnd(object sender, ControllerInteractionEventArgs e)
    {
        hasSlideChanged = false;

        ResetButtons();
    }

    /// <summary>
    /// Changes the colour of the buttons to signify which one is currently being touched by the user
    /// </summary>
    private void UpdateTouchedButton()
    {
        if (touchedAngle <= 180)
        {
            nextButton.GetComponent<Renderer>().material.color = touchedColor;
            previousButton.GetComponent<Renderer>().material.color = untouchedColor;
        }
        else
        {
            nextButton.GetComponent<Renderer>().material.color = untouchedColor;
            previousButton.GetComponent<Renderer>().material.color = touchedColor;
        }
    }

    /// <summary>
    /// Resets the colours of the buttons back to their defaults ones
    /// </summary>
    private void ResetButtons()
    {
        nextButton.GetComponent<Renderer>().material.color = untouchedColor;
        previousButton.GetComponent<Renderer>().material.color = untouchedColor;
    }

    /// <summary>
    /// Loads images at the given path and stores them as textures
    /// </summary>
    /// <param name="path"></param>
    private void LoadSlides(string path)
    {
        UnityEngine.Object[] textures = Resources.LoadAll(path, typeof(Texture2D));

        // Sort the slides alphabetically and cast them to textures
        slides = textures.OrderBy(x => x.name).Select(x => (Texture2D)x).ToArray();

        UpdateSlides();
    }

    /// <summary>
    /// Changes the currently displayed slide to the next one
    /// </summary>
    private void NextSlide()
    {
        if (isStarted)
        {
            results.Times[currentIndex] += (Time.time - startTime);
            startTime = Time.time;
        }

        currentIndex = (currentIndex + 1) % NbSlides;

        UpdateSlides();
    }

    /// <summary>
    /// Changes the currently displayed slide to the previous one
    /// </summary>
    private void PreviousSlide()
    {
        if (isStarted)
        {
            results.Times[currentIndex] += (Time.time - startTime);
            startTime = Time.time;
        }

        // Use a proper modulo formula as C# default is remainder (i.e. goes into negative)
        currentIndex = ((currentIndex - 1) % NbSlides + NbSlides) % NbSlides;

        UpdateSlides();
    }

    /// <summary>
    /// Updates the current slide shown on the screen to the one at the current index
    /// </summary>
    private void UpdateSlides()
    {
        foreach (Renderer renderer in presentationScreens)
        {
            renderer.material.mainTexture = slides[currentIndex];
        }
    }

    private void OnGripClicked(object sender, ControllerInteractionEventArgs e)
    {
        if (interactableObject.IsGrabbed() && interactableObject.GetGrabbingObject() == e.controllerReference.scriptAlias)
        {
            lineRenderer.enabled = true;
        }
    }

    private void OnGripUnclicked(object sender, ControllerInteractionEventArgs e)
    {
        if (interactableObject.IsGrabbed() && interactableObject.GetGrabbingObject() == e.controllerReference.scriptAlias)
        {
            lineRenderer.enabled = false;
        }
    }

    private void OnObjectUngrabbed(object sender, InteractableObjectEventArgs e)
    {
        lineRenderer.enabled = false;
    }
}
