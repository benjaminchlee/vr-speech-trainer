﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class printerScript : MonoBehaviour {
	public Transform printStartPosition;
	public Transform printEndPosition;
	public GameObject paper;
    public GameObject led;
    public Light ledLight;
    public AudioSource audio;

    private Coroutine printCoroutine;

	// Use this for initialization
	void Start () {
		EventSystem.endEvent.AddListener(Print);

	}
	private void OnDestroy()
	{
		EventSystem.endEvent.AddListener(Print);
	}

	void Print(Results results)
	{
        if (printCoroutine != null)
            StopCoroutine(printCoroutine);

        StartCoroutine(AnimatePrinting(results));
	}

    private IEnumerator AnimatePrinting(Results results)
    {
        // Set LED to green
        led.GetComponent<Renderer>().material.color = Color.green;
        led.GetComponent<Renderer>().material.SetColor("_EmissionColor", Color.green);
        ledLight.color = Color.green;

        // Play printing sound
        audio.Play();

        // Wait a second for the "printing" part of the sound to begin
        yield return new WaitForSeconds(1.4f);

        // Remaining part lasts for about 0.8 seconds
        float time = 0.8f;

        GameObject newSheet = Instantiate(paper, printStartPosition.position, printStartPosition.rotation, null);
        newSheet.GetComponentInChildren<AnalysisScreen>().ShowResults(results);
        // Disable collider so it can animate through the printer body
        newSheet.GetComponentInChildren<Collider>().enabled = false;
        newSheet.GetComponentInChildren<Rigidbody>().isKinematic = true;
        // Set it to parent and manipulate local position just in case printer moves while printing
        newSheet.transform.SetParent(transform);

        float elapsed = 0;
        while (elapsed < time)
        {
            newSheet.transform.localPosition = Vector3.Lerp(printStartPosition.localPosition, printEndPosition.localPosition, elapsed / time);

            yield return null;
            elapsed += Time.deltaTime;
        }

        // Force set position if overshoots
        newSheet.transform.localPosition = printEndPosition.transform.localPosition;

        // Reset components
        newSheet.transform.parent = null;
        newSheet.GetComponentInChildren<Collider>().enabled = true;
        newSheet.GetComponentInChildren<Rigidbody>().isKinematic = false;


        // Set LED to red
        led.GetComponent<Renderer>().material.color = Color.red;
        led.GetComponent<Renderer>().material.SetColor("_EmissionColor", Color.red);
        ledLight.color = Color.red;
    }
}
