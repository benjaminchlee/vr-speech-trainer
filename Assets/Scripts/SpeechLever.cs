﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;
using VRTK.Controllables;
using VRTK.Controllables.PhysicsBased;

public class SpeechLever : MonoBehaviour {

    [SerializeField]
    private VRTK_PhysicsRotator physicsRotator;
    [SerializeField]
    private GameObject leverButtonOff;
    [SerializeField]
    private GameObject leverButtonOn;
    [SerializeField]
    private Color deselectedColor = Color.gray;

    private Renderer leverButtonOffRenderer;
    private Renderer leverButtonOnRenderer;
    private Color leverButtonOffColor;
    private Color leverButtonOnColor;
    
	private void Start () {
        if (physicsRotator == null)
            physicsRotator = GetComponentInChildren<VRTK_PhysicsRotator>();

        // Add listeners for when the lever is moved
        physicsRotator.MinLimitReached += OnMinLever;
        physicsRotator.MaxLimitReached += OnMaxLever;
        physicsRotator.ValueChanged += OnValueChanged;

        // Add listeners for when the speech is started/ended outside of this script
        EventSystem.startEvent.AddListener(SpeechStarted);
        EventSystem.endEvent.AddListener(SpeechEnded);

        // Get renderers
        leverButtonOffRenderer = leverButtonOff.GetComponent<Renderer>();
        leverButtonOnRenderer = leverButtonOn.GetComponent<Renderer>();
        // Get original colours
        leverButtonOffColor = leverButtonOffRenderer.material.color;
        leverButtonOnColor = leverButtonOnRenderer.material.color;

        leverButtonOnRenderer.material.color = deselectedColor;
	}

    private void OnValueChanged(object sender, ControllableEventArgs e)
    {
        if (e.interactingTouchScript != null)
            VRTK_ControllerHaptics.TriggerHapticPulse(VRTK_ControllerReference.GetControllerReference(e.interactingTouchScript.gameObject), 0.15f);
    }

    private void OnDestroy()
    {
        physicsRotator.MinLimitReached -= OnMinLever;
        physicsRotator.MaxLimitReached -= OnMaxLever;

        EventSystem.startEvent.RemoveListener(SpeechStarted);
        EventSystem.endEvent.RemoveListener(SpeechEnded);
    }

    private void OnMinLever(object sender, ControllableEventArgs e)
    {
        if (!SpeechManager.Instance.IsInSpeech())
        {
            SpeechManager.Instance.StartSpeech();
        }
    }

    private void OnMaxLever(object sender, ControllableEventArgs e)
    {
        if (SpeechManager.Instance.IsInSpeech())
        {
            SpeechManager.Instance.EndSpeech();
        }
    }

    private void SpeechStarted(float _)
    {
        physicsRotator.SetValue(physicsRotator.angleLimits.minimum);

        leverButtonOffRenderer.material.color = deselectedColor;
        leverButtonOnRenderer.material.color = leverButtonOnColor;
    }

    private void SpeechEnded(Results _)
    {
        physicsRotator.SetValue(physicsRotator.angleLimits.maximum);

        leverButtonOffRenderer.material.color = leverButtonOffColor;
        leverButtonOnRenderer.material.color = deselectedColor;
    }
}
