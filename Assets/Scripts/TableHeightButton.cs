﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

public class TableHeightButton : MonoBehaviour {

    [SerializeField] [Tooltip("The table control script.")]
    private TableHeightControl tableControl;
    [SerializeField] [Tooltip("The direction that this button controls.")]
    private TableHeightControl.Direction direction;
    
    private VRTK_InteractableObject interactableObject;
    private bool isUsed = false;

    private void Start()
    {
        interactableObject = GetComponent<VRTK_InteractableObject>();

        interactableObject.InteractableObjectUsed += OnInteractableObjectUsed;
        interactableObject.InteractableObjectUnused += OnInteractableObjectUnused;
    }
    
    private void OnInteractableObjectUsed(object sender, InteractableObjectEventArgs e)
    {
        tableControl.SetDirection(direction, direction, e.interactingObject);
    }

    private void OnInteractableObjectUnused(object sender, InteractableObjectEventArgs e)
    {
        tableControl.SetDirection(direction, TableHeightControl.Direction.None, e.interactingObject);
    }
}
