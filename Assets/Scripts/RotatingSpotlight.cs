﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatingSpotlight : MonoBehaviour {

    private bool isStarted = false;

	private void Start () {
        EventSystem.startEvent.AddListener(StartSpeech);
        EventSystem.endEvent.AddListener(EndSpeech);
	}

    private void OnDestroy()
    {
        EventSystem.startEvent.RemoveListener(StartSpeech);
        EventSystem.endEvent.RemoveListener(EndSpeech);
    }

    private void StartSpeech(float _)
    {
        isStarted = true;
    }

    private void EndSpeech(Results _)
    {
        isStarted = false;
    }

    private void Update()
    {
        if (isStarted)
        {
            Vector3 direction = (Camera.main.transform.position - transform.position).normalized;
            transform.localRotation = Quaternion.LookRotation(direction) * Quaternion.Euler(Vector3.up * -90);
        }
    }
}
