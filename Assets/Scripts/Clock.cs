﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

public class Clock : MonoBehaviour {

    [SerializeField] [Tooltip("The textmesh for the clock.")]
    private TextMeshPro clockText;

    private bool isColonVisible = true;

    private void Start()
    {
        if (clockText == null)
            clockText = GetComponentInChildren<TextMeshPro>();

        InvokeRepeating("BlinkColon", 1f, 1f);
    }

    private void Update()
    {
        if (isColonVisible)
            clockText.text = DateTime.Now.ToString("HH:mm");
        else
            clockText.text = DateTime.Now.ToString("HH mm");

    }

    private void BlinkColon()
    {
        isColonVisible = !isColonVisible;
    }
}
