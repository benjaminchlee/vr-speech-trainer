﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// The results struct stores the collected results from the audio and
/// movement analysis.
/// </summary>
public struct Results
{
	public int index;
	public float duration;
	public AudioResults audio;
	public MovementResults movement;
    public SlidesResults slides;
}

/// <summary>
/// Responsible for keeping track of speech including logic for ending a speech (putting up the summary page) and starting new speeches
/// </summary>
public class SpeechManager : MonoBehaviour {

    // Singleton pattern
    public static SpeechManager Instance { get; private set; }

    public AudioAnalyser audioAnalyser;
	public MovementManager movementManager;
    public Clicker clickerSlides;

	private int m_currentIndex = 0;
	private float m_startTime;
	bool m_inSpeech = false;

	public bool IsInSpeech() { return m_inSpeech; }

    private void Awake()
    {
        // Singleton pattern
        if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
            return;
        }
        Instance = this;
        DontDestroyOnLoad(gameObject);
    }

    //Functions to start and stop speech
    public void StartSpeech()
	{
		if (m_inSpeech)
		{
			return;
		}
		m_startTime = Time.time;
		m_inSpeech = true;
		EventSystem.startEvent.Invoke(m_startTime);
	}

	public Results GenerateResults()
	{
		// Calculate duration
		float lengthInSeconds = Time.time - m_startTime;
		

		//Generate result struct
		Results results = new Results();
		results.duration = lengthInSeconds;
		if (audioAnalyser != null)
			results.audio = audioAnalyser.GetResults();
		else
			results.audio = new AudioResults();

		if (movementManager != null)
			results.movement = movementManager.Results;
		else
			results.movement = new MovementResults();

        if (clickerSlides != null)
            results.slides = clickerSlides.Results;
        else
            results.slides = new SlidesResults();

		return results;
	}

	public void EndSpeech()
	{
		if (!m_inSpeech) //We need to be in speech
			return;
		if (movementManager != null)
			movementManager.StopLogging();
        if (clickerSlides != null)
            clickerSlides.StopLogging();

		print("GONNA TRY END NOW");
		CrowdScript.Instance.PlayClapping();


		print("ENDING");
		m_inSpeech = false;
		var finalResults = GenerateResults();
		finalResults.index = m_currentIndex;
		m_currentIndex++;
		// Call event with results
		EventSystem.endEvent.Invoke(finalResults);

        int score = EvaluateSpeech();
        if (score == 1)
            AudioManager.Instance.PlaySoftClapping();
        else if (score == 2)
            AudioManager.Instance.PlayMediumClapping();
        else if (score == 3)
            AudioManager.Instance.PlayLoudClapping();
	}

    /// <summary>
    /// Evaluates the current speech, whether in progress or completed, and returns a score of 1, 2, or 3. 1 indicates a bad speech, 2 indicates an average speech, and 3 indicates a great speech.
    /// The legitimacy of this result is questionable for very short speeches (e.g. 5 seconds).
    /// 
    /// The user is given a rating of 1, 2, or 3 for every category. These ratings are then averaged and rounded to the nearest whole number which then forms the final score. Note that because
    /// of this, majority of scores will be given a 2. This is somewhat the intention.
    /// 
    /// If there is category that is somehow illegible (e.g. no words spoken), that category is skipped.
    /// </summary>
    /// <returns>An integer of value 1, 2, or 3 which indicates the quality of the user's speech.</returns>
    public int EvaluateSpeech()
    {
        Results results = GenerateResults();
        List<int> scores = new List<int>();
        float length = results.duration;

        // Evaluate audio
        AudioResults audio = results.audio;
        int wordCount = audio.wordsPerMinute.Sum();

        /// WORDS PER MINUTE
        /// Average WPM is between 125 and 150. Anything outside of this range is considered bad.
        /// 
        /// Below 110 : 1
        /// 110 - 125 : 2
        /// 125 - 150 : 3
        /// 150 : 165 : 2
        /// Above 165 : 1
        float wpm = wordCount / length;
        if (wpm < 110)
            scores.Add(1);
        else if (110 <= wpm && wpm < 125)
            scores.Add(2);
        else if (125 <= wpm && wpm < 150)
            scores.Add(3);
        else if (150 <= wpm && wpm < 165)
            scores.Add(2);
        else if (165 <= wpm)
            scores.Add(1);

        /// FILLER WORDS / HESITATION PERCENTAGE
        /// Lower hesitation the better.
        /// 
        /// Below 2% : 3
        /// 2% - 5%  : 2
        /// Above 5% : 1
        try
        {
            float hesitation = audio.hesitationCount / wordCount;
            if (hesitation < 0.02f)
                scores.Add(3);
            else if (0.02f <= hesitation && hesitation < 0.05f)
                scores.Add(2);
            else if (0.05f <= hesitation)
                scores.Add(1);
        }
        catch (DivideByZeroException e)
        {
            // Skip if wordCount == 0;   
        }

        // Evaluate movement
        MovementResults movement = results.movement;

        /// AUDIENCE ENGAGEMENT PERCENTAGE
        /// The more the user looks at the audience the better.
        /// 
        /// Below 50% : 1
        /// 50% - 80% : 2
        /// Above 80% : 3
        float engagement = movement.AudienceGazeDuration / length;
        if (engagement < 0.5f)
            scores.Add(1);
        else if (0.5f <= engagement && engagement < 0.8f)
            scores.Add(2);
        else if (0.8f <= engagement)
            scores.Add(3);

        /// OVERALL MOVEMENT SPEED OF HEAD
        /// The more body movement the better.
        /// 
        /// Below 0.01 m/s  : 1
        /// 0.01 - 0.03 m/s : 2
        /// Above 0.03 m/s  : 3
        float speed = movement.HeadDistanceTravelled / length;
        if (speed < 0.01f)
            scores.Add(1);
        else if (0.01f <= speed && speed < 0.03f)
            scores.Add(2);
        else if (0.03 <= speed)
            scores.Add(3);

        float averageScore = (float)scores.Average();
        int overallScore = Mathf.RoundToInt(averageScore);

        Debug.Log(string.Format("Evaluated scores: [{0}]", string.Join(",", scores)));
        Debug.Log(string.Format("Evaluated average score: {0}, Evaluated overall score: {1}", averageScore, overallScore));

        return overallScore;
    }
}
