﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DistractionManager : MonoBehaviour {
    
    [SerializeField] [Tooltip("The minimum time in seconds between ambient distraction triggers.")]
    private float minAmbientTime = 3;
    [SerializeField] [Tooltip("The maximum time in seconds between ambient distraction triggers.")]
    private float maxAmbientTime = 5;

    [SerializeField] [Tooltip("The minimum time in seconds between reactive distraction triggers.")]
    private float minReactiveTime = 10;
    [SerializeField] [Tooltip("The maximum time in seconds between reactive distraction triggers.")]
    private float maxReactiveTime = 20;

	private Coroutine ambientCoroutine;
    private Coroutine reactiveCoroutine;

    // Singleton pattern
    public static DistractionManager Instance { get; private set; }

    private void Awake()
    {
        // Singleton pattern
        if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
            return;
        }
        Instance = this;
        DontDestroyOnLoad(gameObject);
    }

    private void Start()
    {
        //Set up event listeners
        EventSystem.startEvent.AddListener(StartAmbient);
        EventSystem.startEvent.AddListener(StartReactive);
        EventSystem.endEvent.AddListener(EndAmbient);
        EventSystem.endEvent.AddListener(EndReactive);
    }

    private void StartAmbient(float _)
    {
        if (ambientCoroutine != null)
        {
            StopCoroutine(ambientCoroutine);
        }

        ambientCoroutine = StartCoroutine(LoopAmbient());
    }

    private IEnumerator LoopAmbient()
    {
        while (true)
        {
            // Wait for a random amount of time within the intervals
            yield return new WaitForSeconds(Random.Range(minAmbientTime, maxAmbientTime));

            TriggerAmbient();
        }
    }
    
    /// <summary>
    /// Ambient distractions always occur regardless on user performance
    /// </summary>
    private void TriggerAmbient()
    {
        // Trigger animations
        Debug.Log("Ambient animations triggered");

        // Trigger sounds
        Debug.Log("Ambient sounds triggered");

        int rand = Random.Range(0, 100);

        // 5% chance of coughing
        if (rand < 20)
        {
			CrowdScript.Instance.PlayCoughing();
            AudioManager.Instance.PlayCoughing();
			

		}
        // 15% chance of shuffling
        else if (rand < 20)
        {
            AudioManager.Instance.PlayShuffling();
        }
    }

    private void EndAmbient(Results _)
    {
        if (ambientCoroutine != null)
        {
            StopCoroutine(ambientCoroutine);
        }
    }

    private void StartReactive(float _)
    {
        if (reactiveCoroutine != null)
        {
            StopCoroutine(reactiveCoroutine);
        }

        reactiveCoroutine = StartCoroutine(LoopReactive());
    }

    private IEnumerator LoopReactive()
    {
        while (true)
        {
            // Wait for a random amount of time within the intervals
            yield return new WaitForSeconds(Random.Range(minReactiveTime, maxReactiveTime));

            TriggerReactive();
        }
    }

    /// <summary>
    /// Reactive distractions are triggered based upon the user's performance
    /// </summary>
    private void TriggerReactive()
    {
        int score = SpeechManager.Instance.EvaluateSpeech();

        // ADD CONDITIONS HERE
        Debug.Log("Reactive conditions checked");

        if (score == 1)
        {
            AudioManager.Instance.SetCrowdTalkative();
			CrowdScript.Instance.SetBadResponse();
        }
        else if (score == 2)
        {
            AudioManager.Instance.SetCrowdWhispering();
			CrowdScript.Instance.SetMildResponse();
        }
        else if (score == 3)
        {
            AudioManager.Instance.SetCrowdSilent();
			CrowdScript.Instance.SetGoodResponse();
        }

    }

    private void EndReactive(Results _)
    {
        if (reactiveCoroutine != null)
        {
            StopCoroutine(reactiveCoroutine);
        }
    }
}
