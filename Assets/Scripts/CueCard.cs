﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using VRTK;

public class CueCard : MonoBehaviour {

    [SerializeField] [Tooltip("A text file of the user's notes to be displayed in an in-game object. It must be saved in UTF-8.")]
    private TextAsset notesAsset;
    [SerializeField] [Tooltip("The TextMesh that displays the user's notes.")]
    private TextMeshPro notesTextMesh;
    [SerializeField] [Tooltip("The TextMesh that displays the page number of the user's notes.")]
    private TextMeshPro pageNumberTextMesh;
    [SerializeField] [Tooltip("The delimiter(s) used to distinguish between pages.")]
    private string[] delimiters = new string[] { "///" };
    [SerializeField] [Tooltip("The strength of the vibration when the user changes the page. This value must be between 0 and 1.")]
    private float vibrationStrength = 0.4f;

    private string[] notes;
    public string[] Notes
    {
        get { return notes; }
    }
    public int NbPages
    {
        get { return notes.Length; }
    }
    public string ActiveNote
    {
        get { return notes[currentIndex]; }
    }

    private int currentIndex = 0;

    private VRTK_InteractableObject interactableObject;
    private VRTK_ControllerEvents leftControllerEvents;
    private VRTK_ControllerEvents rightControllerEvents;

    private bool hasPageChanged = false;

    private void Start()
    {
        interactableObject = GetComponent<VRTK_InteractableObject>();
        leftControllerEvents = VRTK_DeviceFinder.GetControllerLeftHand().GetComponent<VRTK_ControllerEvents>();
        rightControllerEvents = VRTK_DeviceFinder.GetControllerRightHand().GetComponent<VRTK_ControllerEvents>();

        // Register events
        leftControllerEvents.TouchpadTouchStart += OnTouchpadAxisChanged;
        rightControllerEvents.TouchpadTouchStart += OnTouchpadAxisChanged;
        leftControllerEvents.TouchpadAxisChanged += OnTouchpadAxisChanged;
        rightControllerEvents.TouchpadAxisChanged += OnTouchpadAxisChanged;
        leftControllerEvents.TouchpadTouchEnd += OnTouchpadReleased;
        rightControllerEvents.TouchpadTouchEnd += OnTouchpadReleased;

        LoadText(notesAsset.text);
    }

    /// <summary>
    /// Navigates the cue card to the next page when the touchpad was released on the right hand side of the pad, or to the previous page when on the 
    /// left hand side of the pad
    /// </summary>
    private void OnTouchpadAxisChanged(object sender, ControllerInteractionEventArgs e)
    {
        if (interactableObject.IsGrabbed() && interactableObject.GetGrabbingObject() == e.controllerReference.scriptAlias)
        {
            if (0.8 <= e.touchpadAxis.x)
            {
                if (!hasPageChanged)
                {
                    NextPage();
                    hasPageChanged = true;
                    VRTK_ControllerHaptics.TriggerHapticPulse(e.controllerReference, vibrationStrength);
                }
            }
            else if (e.touchpadAxis.x <= -0.8)
            {
                if (!hasPageChanged)
                {
                    PreviousPage();
                    hasPageChanged = true;
                    VRTK_ControllerHaptics.TriggerHapticPulse(e.controllerReference, vibrationStrength);
                }
            }
            else
            {
                hasPageChanged = false;
            }
        }
    }
    
    private void OnTouchpadReleased(object sender, ControllerInteractionEventArgs e)
    {
        hasPageChanged = false;
    }

    /// <summary>
    /// Parses the given string into an array whereby each element corresponds to a page in the user's notes
    /// </summary>
    /// <param name="text">The user's notes</param>
    public void LoadText(string text)
    {
        string[] split = text.Split(delimiters, System.StringSplitOptions.RemoveEmptyEntries);
        notes = split.Select(x => x.Trim()).ToArray();

        UpdatePage();
    }

    /// <summary>
    /// Changes the currently displayed page to the next one
    /// </summary>
    public void NextPage()
    {
        currentIndex = (currentIndex + 1) % NbPages;

        UpdatePage();
    }

    /// <summary>
    /// Changes the currently displayed page to the previous one
    /// </summary>
    public void PreviousPage()
    {
        // Use a proper modulo formula as C# default is remainder (i.e. goes into negative)
        currentIndex = ((currentIndex - 1) % NbPages + NbPages) % NbPages;

        UpdatePage();
    }

    /// <summary>
    /// Updates the text and page number on the cue card based on the current index
    /// </summary>
    private void UpdatePage()
    {
        notesTextMesh.text = notes[currentIndex];
        pageNumberTextMesh.text = string.Format("({0}/{1})", currentIndex + 1, NbPages + 1);
    }
}
