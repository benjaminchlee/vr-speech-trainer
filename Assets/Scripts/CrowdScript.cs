﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct PersonData
{
	public GameObject person;
	public Transform head;
	public Transform headTarget;

}
public class CrowdScript : MonoBehaviour {
	// Singleton pattern
	public static CrowdScript Instance { get; private set; }
	
	private void Awake()
	{
		// Singleton pattern
		if (Instance != null && Instance != this)
		{
			Destroy(gameObject);
			return;
		}
		Instance = this;
		DontDestroyOnLoad(gameObject);
	}


	public Transform[] targets;
	public Transform seatParent;
	public GameObject[] peoplePrefabs;
	public Vector3 Offset = new Vector3(0,-1.0f,0);
	public Quaternion RotationOffset = Quaternion.Euler(new Vector3(0, 180, 0));

	PersonData[] people;
	Transform[] peoplesHeads; //I apologise for this hackery, I just want to get this done!
	// Use this for initialization
	void Start () {

		//Make list of seats (that we don't have to feed in)
		List<Transform> seats = new List<Transform>();
		for(int i = 0; i < seatParent.childCount; i++)
		{
			seats.Insert(Random.Range(0, seats.Count),seatParent.GetChild(i).transform);
		}

		people = new PersonData[peoplePrefabs.Length];
		
		//Spawn crowd
		for (int i = 0; i < people.Length; i++)
		{
			var person = SpawnPerson(peoplePrefabs[i], seats[i]);
			people[i] = new PersonData
			{
				person = person,
				head = person.transform.Find("mixamorig:Hips").Find("mixamorig:Spine").Find("mixamorig:Spine1").Find("mixamorig:Spine2").Find("mixamorig:Neck").Find("mixamorig:Head"),
				headTarget = targets[Random.Range(0, targets.Length)]
			};
		}

	}
	GameObject SpawnPerson(GameObject person, Transform seat)
	{
		GameObject newBorn = Instantiate(person,transform);
		newBorn.transform.position += (seat.position+Offset) - newBorn.transform.position;
		//person.transform.Find("mixamorig:Hips").position = seat.position;
		newBorn.transform.rotation = RotationOffset;
		return newBorn;
	}
	private void LateUpdate()
	{
		for(int i = 0; i < people.Length; i++)
		{
			int personState = people[i].person.GetComponent<Animator>().GetInteger("State");
			if (personState != -1 && personState != 1)
			{
				people[i].head.LookAt(people[i].headTarget);
			}
		}
	}
	public void PlayClapping()
	{
		for(int i = 0; i < people.Length; i++)
		{

			people[i].person.GetComponent<Animator>().SetBool("Clapping", true);
			
		}
		StartCoroutine(StopClapping(3.0f));
	}
	public IEnumerator StopClapping(float t)
	{
		yield return new WaitForSeconds(t);
		for (int i = 0; i < people.Length; i++)
		{
			people[i].person.GetComponent<Animator>().SetBool("Clapping", false);
		}

	}
	public void PlayCoughing()
	{
		int luckyPneumoniaPatient = Random.Range(0, people.Length);
		people[luckyPneumoniaPatient].person.GetComponent<Animator>().SetBool("Coughing", true);
		StartCoroutine(StopCoughing(2.0f, luckyPneumoniaPatient));

	}
	public IEnumerator StopCoughing(float t,int index)
	{
		yield return new WaitForSeconds(t);
		people[index].person.GetComponent<Animator>().SetBool("Coughing", false);

	}
	public IEnumerator DelayedAnimation(float t, int index,int state)
	{
		yield return new WaitForSeconds(t);
		people[index].person.GetComponent<Animator>().SetInteger("State", state);

	}
	public void SetBadResponse()
	{
		for(int i = 0; i < people.Length; i++)
		{
			if(Random.Range(0,2) == 1)
			{
				StartCoroutine(DelayedAnimation(Random.value*10 / 5,i, Random.Range(-2, 0)));
			}
		}
		
	}
	public void SetMildResponse()
	{
		for (int i = 0; i < people.Length; i++)
		{
			if(Random.Range(0,2) == 1)
			{
				StartCoroutine(DelayedAnimation(Random.value * 10 / 5, i, Random.Range(-1, 1)));
			}
			
		}

	}
	public void SetGoodResponse()
	{
		for (int i = 0; i < people.Length; i++)
		{
			if(Random.Range(0,2) == 1)
			{
				StartCoroutine(DelayedAnimation(Random.value * 10 / 5, i,0));
			}
			
		}

	}
}
