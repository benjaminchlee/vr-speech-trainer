﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[Serializable]
public class OnStartSpeech : UnityEvent<float>{}
[Serializable]
public class OnEndSpeech : UnityEvent<Results> { }

/// <summary>
/// The Event System class is a static class which holds all the events so
/// that they are accessible across the program.
/// </summary>
public class EventSystem : MonoBehaviour {

	public static OnStartSpeech startEvent = new OnStartSpeech();
	public static OnEndSpeech endEvent = new OnEndSpeech();

}
