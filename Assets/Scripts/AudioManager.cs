﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class AudioManager : MonoBehaviour {

    // Singleton pattern
    public static AudioManager Instance { get; private set; }

    [SerializeField] [Tooltip("The audiosource which plays the ambient sounds from the script.")]
    private AudioSource ambientSource;
    [SerializeField] [Tooltip("The audiosource which plays the crowd ambience from the script.")]
    private AudioSource crowdSource;
    [SerializeField] [Tooltip("The audiosource which plays the sound effects from the script.")]
    private AudioSource effectsSource;

    [SerializeField] [Tooltip("The audioclip for crowd ambience to be played when not in a speech.")]
    private AudioClip talkingAmbience;
    [SerializeField] [Tooltip("The audioclip for lecture theatre ambience to be played when in a speech.")]
    private AudioClip lectureAmbience;

    [SerializeField] [Tooltip("The audioclips for coughing.")]
    private AudioClip[] coughing;
    [SerializeField] [Tooltip("The volume of the coughing.")]
    private float coughingVolume = 0.4f;

    [SerializeField] [Tooltip("The audioclips for shuffling.")]
    private AudioClip[] shuffling;
    [SerializeField] [Tooltip("The volume of the shuffling.")]
    private float shufflingVolume = 0.25f;
    
    [SerializeField] [Tooltip("The audioclip for the loud clapping.")]
    private AudioClip loudClapping;
    [SerializeField] [Tooltip("The audioclip for the medium clapping.")]
    private AudioClip mediumClapping;
    [SerializeField] [Tooltip("The audioclip for the soft clapping.")]
    private AudioClip softClapping;
    [SerializeField] [Tooltip("The volume of the clapping.")]
    private float clappingVolume = 0.75f;


    private float crowdMaxVolume;
    private Coroutine crowdCoroutine;

    private void Awake()
    {
        // Singleton pattern
        if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
            return;
        }
        Instance = this;
        DontDestroyOnLoad(gameObject);
    }

    private void Start()
    {
        // Add listeners for when the speech is started/ended
        EventSystem.startEvent.AddListener(SpeechStarted);
        EventSystem.endEvent.AddListener(SpeechEnded);

        ambientSource.clip = lectureAmbience;
        ambientSource.loop = true;
        ambientSource.Play();

        crowdMaxVolume = crowdSource.volume;
        crowdSource.clip = talkingAmbience;
        // Pick a random time in the first parts of the clip
        crowdSource.time = Mathf.Round((Random.Range(0, 0.5f) * talkingAmbience.length));
        crowdSource.loop = true;
        crowdSource.Play();
    }

    public void PlayCoughing()
    {
        effectsSource.Stop();
        int index = Random.Range(0, coughing.Length);
        effectsSource.clip = coughing[index];
        effectsSource.volume = coughingVolume;
        effectsSource.Play();
    }

    public void PlayShuffling()
    {
        effectsSource.Stop();
        int index = Random.Range(0, shuffling.Length);
        effectsSource.clip = shuffling[index];
        effectsSource.volume = shufflingVolume;
        effectsSource.Play();
    }

    public void PlayLoudClapping()
    {
        effectsSource.Stop();
        effectsSource.clip = loudClapping;
        effectsSource.volume = clappingVolume;
        effectsSource.PlayDelayed(Random.Range(0.25f, 0.5f));
		
    }

    public void PlayMediumClapping()
    {
        effectsSource.Stop();
        effectsSource.clip = mediumClapping;
        effectsSource.volume = clappingVolume;
        effectsSource.PlayDelayed(Random.Range(0.45f, 0.9f));
    }

    public void PlaySoftClapping()
    {
        effectsSource.Stop();
        effectsSource.clip = softClapping;
        effectsSource.volume = clappingVolume;
        effectsSource.PlayDelayed(Random.Range(1, 1.5f));
    }
    
    public void SetCrowdSilent()
    {
        if (crowdCoroutine != null)
            StopCoroutine(crowdCoroutine);

        crowdCoroutine = StartCoroutine(FadeCrowdAmbience(3f, 0));
    }

    public void SetCrowdWhispering()
    {
        if (crowdCoroutine != null)
            StopCoroutine(crowdCoroutine);

        crowdCoroutine = StartCoroutine(FadeCrowdAmbience(5f, 0.03f));
    }

    public void SetCrowdTalkative()
    {
        if (crowdCoroutine != null)
            StopCoroutine(crowdCoroutine);

        crowdCoroutine = StartCoroutine(FadeCrowdAmbience(5f, 0.1f));
    }


    private void SpeechStarted(float _)
    {
        if (crowdCoroutine != null)
            StopCoroutine(crowdCoroutine);

        crowdCoroutine = StartCoroutine(FadeCrowdAmbience(3.5f, 0));
    }

    private void SpeechEnded(Results _)
    {
        if (crowdCoroutine != null)
            StopCoroutine(crowdCoroutine);

        crowdCoroutine = StartCoroutine(FadeCrowdAmbience(12f, 1));
    }

    private IEnumerator FadeCrowdAmbience(float fade, float newVolume)
    {
        float elapsed = 0;
        float startVolume = crowdSource.volume;

        while (elapsed < fade)
        {
            crowdSource.volume = Mathf.Lerp(startVolume, newVolume, elapsed / fade);
            elapsed += Time.deltaTime;
            yield return null;
        }

        // Force set audio volume if it over/under shoots
        crowdSource.volume = newVolume;
    }
}
